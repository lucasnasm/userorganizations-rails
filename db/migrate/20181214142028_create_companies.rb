class CreateCompanies < ActiveRecord::Migration[5.2]
  def change
    create_table :companies do |t|
      
      t.string :company_name, null: false, default: '', unique: true

      t.timestamps
    end
  end
end
