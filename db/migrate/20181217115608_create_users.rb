class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name , null: false, default: ''
      t.string :email ,   null: false, default: '', unique: true
      t.string :encrypted_password, null: false, default: ''
      t.references :company, foreign_key: true 
      t.references :roles, foreign_key: true
      t.timestamps
    end
  end
end
