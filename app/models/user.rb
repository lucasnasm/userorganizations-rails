class User < ApplicationRecord
    
    belongs_to :company
    accepts_nested_attributes_for :company

    validates_presence_of :name, :email, :encrypted_password
    
end
